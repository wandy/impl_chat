package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
)

func Request(uri string, token string, payload []byte, method string, _other map[string]string) (int, []byte, error) {
	buf := bytes.NewBuffer(payload)
	// Create a new request using http
	req, err := http.NewRequest(method, uri, buf)
	if err != nil {
		log.Printf("error when create new request => %s", err)
	}
	// add authorization header to the req
	if token != "" {
		req.Header.Add("Authorization", token)
	}
	if method != http.MethodGet {
		req.Header.Add("Content-Type", "application/json")
	}

	if _other["QiscusAppId"] != "" {
		req.Header.Add("Qiscus-App-Id", _other["QiscusAppId"])
	}

	// Send req using http Client
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("Error on response. %s \n ", err)
		return http.StatusInternalServerError, nil, err
	}
	defer resp.Body.Close()

	result, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("Error while reading the response bytes:", err)
	}
	return resp.StatusCode, result, nil
}
