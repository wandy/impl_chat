package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
)

const (
	baseUrl            = "https://multichannel.qiscus.com"
	admEmail           = "wandy.irawan17@gmail.com"
	admPass            = "fh9kGT07T9Yl"
	qiscusAppId        = "qwxxr-lzvfssx5tncgcji"
	qiscussAccessToken = "485a68cbebe175579da0b24471bdf866"
)

//var
var (
	rdb   *redis.Client
	token string
)

//logic
func loginAuth(rdb *redis.Client, c *gin.Context) User {
	var auth Auth
	uri := baseUrl + "/api/v1/auth"
	payload := AuthPayload{Email: admEmail, Password: admPass}
	bitPayload, err := json.Marshal(payload)
	if err != nil {
		panic(err)
	}
	_, body, _ := Request(uri, "", bitPayload, "POST", nil)
	// log.Println(string(body))

	err = json.Unmarshal(body, &auth)
	if err != nil {

		panic(err)
	}
	log.Println(auth)
	return auth.Data.User
}

// func for add id agent to redis chache
func createAssignAgentRedis(rdb *redis.Client, c *gin.Context, agentId string) {
	var value string
	key := "AssignmentAgent"
	data := getAssignAgentRedis(rdb, c)
	if data == "" {
		value = agentId
	} else {
		value = data + "#" + agentId
	}
	rdb.Set(c.Request.Context(), key, value, 0)
}

// func for get id agent from redis
func getAssignAgentRedis(rdb *redis.Client, c *gin.Context) string {
	key := "AssignmentAgent"
	dataRds, err := rdb.Get(c.Request.Context(), key).Result()
	if err == redis.Nil {
		return dataRds
	} else if err != nil {
		panic(err)
	} else {
		return ""
	}
}

// func for delete id agent from redis
func deleteAssignAgentRedis(rdb *redis.Client, c *gin.Context) {

}

func generateToken(rdb *redis.Client, c *gin.Context) string {
	key := "user"
	var user User
	userRds, err := rdb.Get(c.Request.Context(), key).Result()
	if userRds != "" {
		// fmt.Println("key accessToken does not exist")
		err = json.Unmarshal([]byte(userRds), &user)
		if err != nil {
			panic(err)
		}
		token = user.AuthenticationToken
	}

	if err != nil && err != redis.Nil {
		panic(err)
	}

	user = loginAuth(rdb, c)
	token = user.AuthenticationToken
	userByte, _ := json.Marshal(user)
	err = rdb.SetEX(c.Request.Context(), key, userByte, 60*time.Minute).Err()
	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	return token
}

// fouter
func pong(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "pong",
	})
}

func otherAgent(c *gin.Context) {
	roomId := c.Query("room_id")
	limit := c.Query("limit")
	uri := baseUrl + fmt.Sprintf("/api/v2/admin/service/other_agents?room_id=%s&limit=%s", roomId, limit)
	token := generateToken(rdb, c)
	other := make(map[string]string)
	other["QiscusAppId"] = qiscusAppId
	_, otherAgent, _ := Request(uri, token, nil, "GET", other)
	fmt.Println(string(otherAgent))
	c.JSON(200, gin.H{
		"agent": "unimplementad",
	})

}

func assignAgent(c *gin.Context) {
	var assignAgent AssignAgentPayload
	if err := c.ShouldBindJSON(&assignAgent); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "Bad request",
		})
		return
	}
	// uri := baseUrl + "/api/v1/admin/service/assign_agent"

	// token := generateToken(rdb, c)
	// other := make(map[string]string)
	// other["QiscusAppId"] = qiscusAppId
	// _, assignAgent, _ := Request(uri, token, nil, "GET", other)
	// fmt.Println(string(assignAgent))
	agenId := strconv.Itoa(assignAgent.AgentID)
	createAssignAgentRedis(rdb, c, agenId)
	c.JSON(200, gin.H{
		"agent": "Not implementation yaet",
	})
}

//main func
func main() {
	os.Setenv("PORT", "3000")
	port := os.Getenv("PORT")
	rdb = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	if port == "" {
		log.Fatal("$PORT must be set")
	}

	r := gin.Default()
	r.GET("/ping", pong)
	r.GET("/other_agent", otherAgent)
	r.POST("/assign_agent", assignAgent)

	r.Run(":" + port)
}
