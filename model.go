package main

type Auth struct {
	Data struct {
		User `json:"user"`
	} `json:"data"`
}

type User struct {
	ID                  int    `json:"id"`
	Name                string `json:"name"`
	Email               string `json:"email"`
	AuthenticationToken string `json:"authentication_token"`
	SdkEmail            string `json:"sdk_email"`
	SdkKey              string `json:"sdk_key"`
}

type AuthPayload struct {
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

type AssignAgentPayload struct {
	AgentID int    `json:"agent_id,omitempty"`
	RoomID  string `json:"room_id,omitempty"`
}
